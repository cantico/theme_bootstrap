<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';





/**
 * @param string $str
 *
 * @return string
 */
function theme_bootstrap_translate($str)
{
    return bab_translate($str, 'theme_bootstrap');
}




function theme_bootstrap_redirect($url, $message = null)
{
	global $babBody;

	if (null === $url) {
		die('<script type="text/javascript">history.back();</script>');
	}

	header('Location: ' . $url);
	die;
}


/**
 *
 * @param string        $text
 * @param Widget_Widget $widget
 */
function theme_bootstrap_LabelledWidget($text, $widget)
{
    $W = bab_Widgets();
    $label = $W->Label($text);
    $label->setSizePolicy('widget-15em');
    $label->setAssociatedWidget($widget);
    $label->colon();

    $labelledWidget =  $W->FlowItems($label, $widget);
    $labelledWidget->setHorizontalSpacing(1, 'em');
 
    return $labelledWidget;
}



/**
 * 
 * @param string		$text
 * @param Widget_Widget	$widget
 * @param string		$name
 * @param bool			$checked
 * @return unknown
 */
function theme_bootstrap_OptionalLabelledWidget($text, $widget, $name, $checked)
{
    $W = bab_Widgets();
    $label = $W->Label($text);
    $label->setSizePolicy('widget-10em');
    $label->setAssociatedWidget($widget);
    $label->colon();
       
    $checkbox = $W->CheckBox();
    $checkbox->setAssociatedDisplayable($widget);
    $checkbox->setName($name);
    $checkbox->setUncheckedValue('0');
    $checkbox->setCheckedValue($widget->getId());
    if ($checked) {
        $checkbox->setValue($widget->getId());
    }
    $checkbox->setSizePolicy('widget-5em');
    $labelledWidget = $W->FlowItems($label, $checkbox, $widget);
    $labelledWidget->setHorizontalSpacing(1, 'em');

    return $labelledWidget;
}





function theme_bootstrap_shorten_values(&$defaultValues, $reg)
{
	$values = array();

	foreach ($defaultValues as $key => $value){
		$values[$key] = $reg->getValue($key, $value);
	}

	return $values;
}


/**
 * The folder path where the compiled css file will be stored.
 * @return string
 */
function theme_bootstrap_getCompiledCssPath()
{
    $rootPath = realpath('.');
    $addon = bab_getAddonInfosInstance('theme_bootstrap');
    $compiledCssPath = $rootPath . '/images/' . $addon->getRelativePath();
    return $compiledCssPath;
}


/**
 * 
 * @param string $configuration
 * @return array
 */
function theme_bootstrap_getLessVariables($configuration = 'global')
{
// 	$registry = bab_getRegistryInstance();
// 	$registry->changeDirectory('/theme_bootstrap/' . $configuration);

	$defaultValues = array(
        'headerBackgroundColor' => '#26AADA',
		'mainColor'				=> '#DDDDDD',

        'bannerImage'           => "''",
        'logoImage'             => "''",
		'faviconImage'          => "''",
				
        'maxWidth'              => '0',
	    'headerHeight'          => '0'
	);
	
	$values = array();
	
	foreach ($defaultValues as $key => $value) {
	    $values[$key] = theme_bootstrap_getConfigurationValue('/theme_bootstrap/' . $configuration, $key, $value);
//		$values[$key] = $registry->getValue($key, $value);
	}
	

	return $values;
}





function theme_bootstrap_defineTxtColor($bckcolor) {
	if(!$bckcolor) {
		return "#000";
	}
	else {
		$c = theme_bootstrap_hex2rgb($bckcolor);
		
		if ( ($c['r'] + $c['g'] + $c['b']) > 382) {
			return "#000";
		}
		else
			return "#FFF";
	}
}




function theme_bootstrap_getColor($name, $configuration, $defaultValues, $dependances)
{
    $color = new Color;
    
    if (!isset($dependances[$name]) ||
        !isset($configuration[$dependances[$name]]) ||
        $configuration[$dependances[$name]] != '0') {
        $color->setColor($configuration[$name]);
    }
    else {
        if (isset($defaultValues[$name]) && $defaultValues[$name] != null) {
            if (is_string($defaultValues[$name]))
                $color->setColor($defaultValues[$name]);
            else {
                return $defaultValues[$name];
            }
        }
    }
        
    return $color;
}





function theme_bootstrap_setRegisterValue($name, $value, $registry, $configuration)
{
    // echo $name.' : '.$configuration[$name].'<br />';
    
    if (isset($configuration[$name]) && (is_string($configuration[$name]))) {
        $registry->setKeyValue($name, $value);
    }
}






/**
 * 
 * @param string $registryDirectory
 * @param string $key
 * @param string $default
 * @return mixed
 */
function theme_bootstrap_getConfigurationValue($registryDirectory, $key, $default = null)
{
    if (isset($GLOBALS[$registryDirectory . '/' . $key])) {
        // The global variable can override
        $value = $GLOBALS[$registryDirectory . '/' . $key];
    } else {
    
        // Selection du registre du theme
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory($registryDirectory);
    
        $value = $registry->getValue($key, $default);
    }
    
    return $value;
}




//or hex color to rgb

function theme_bootstrap_hex2rgb($c)
{ 
   if(!$c) return false; 
   $c = trim($c); 
   $out = false; 
  if(preg_match("/^[0-9ABCDEFabcdef\#]+$/i", $c)){ 
      $c = str_replace('#','', $c); 
      $l = strlen($c) == 3 ? 1 : (strlen($c) == 6 ? 2 : false); 

      if($l){ 
         unset($out); 
         $out['r'] = hexdec(substr($c, 0,1*$l)); 
         $out['g'] = hexdec(substr($c, 1*$l,1*$l)); 
         $out['b'] = hexdec(substr($c, 2*$l,1*$l)); 
      }else $out = false; 
              
   }else $out = false; 
          
   return $out; 
} 
