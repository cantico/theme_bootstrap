;<?php /*

[general]
name				="theme_bootstrap"
version				="0.1.0"
addon_type			="THEME"
encoding			="UTF-8"
mysql_character_set_database="latin1,utf8"
delete				=1
ov_version			="8.5.0"
php_version			="5.1.3"
mysql_version		="4.0"
author				="cantico"
addon_access_control = "0"
configuration_page  ="configuration"
icon                ="icon.png"

[addons]
jquery				="1.9"
widgets				="1.0"
LibFileManagement 	="0.2.14"
LibLess             ="0.3.8.0"

;*/ ?>
