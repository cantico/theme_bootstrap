<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';

bab_functionality::includefile('Ovml/Function');

/**
 * <OFThemeBootstrapGetRegistryValue key="" default="">
 */
class Func_Ovml_Function_ThemeBootstrapGetRegistryValue extends Func_Ovml_Function
{
    public function toString()
    {
        $registryDirectory = '/' . $GLOBALS['babSkin'] . '/global';
        $key = '';
        $saveas = '';
        $default = null;

        if (count($this->args)) {
            // Récupération des arguments
            foreach ($this->args as $name => $value) {
                switch (mb_strtolower(trim($name))) {
                    case 'registry':
                        $registryDirectory = $value;
                        break;
                    case 'key':
                        $key = $value;
                        break;
                    case 'default':
                        $default = $value;
                        break;
                    case 'saveas':
                        $saveas = $value;
                    break;
                }
            }

            if (!$key) {
                return '';
            }

            $value = theme_bootstrap_getConfigurationValue($registryDirectory, $key, $default);

//             if (isset($GLOBALS[$registryDirectory . '/' . $key])) {
//                 // The global variable can override
//                 $value = $GLOBALS[$registryDirectory . '/' . $key];
//             } else {

//                 // Selection du registre du theme
//                 $registry = bab_getRegistryInstance();
//                 $registry->changeDirectory($registryDirectory);

//                 $value = $registry->getValue($key, $default);
//             }

            if ($saveas) {
                // Insertion dans une variable OvML
                $this->gctx->push($saveas, $value);
                return '';
            }

            return $value;
        }
    }
}


/**
 * <OFThemeBootstrapImageBase64 file="">
 */
class Func_Ovml_Function_ThemeBootstrapImageBase64 extends Func_Ovml_Function
{
    public function toString()
    {
        $registryDirectory = '/' . $GLOBALS['babSkin'] . '/global';
        $key = '';
        $saveas = '';
        $default = null;

        if (count($this->args)) {
            // Récupération des arguments
            foreach ($this->args as $name => $value) {
                switch (mb_strtolower(trim($name))) {
                    case 'file':
                        $filename = trim($value, "'");
                        break;
                    case 'saveas':
                        $saveas = $value;
                        break;
                }
            }

            if (!$filename) {
                return '';
            }

            $file = file_get_contents($filename);
            $value = base64_encode($file);


            if ($saveas) {
                // Insertion dans une variable OvML
                $this->gctx->push($saveas, $value);
                return '';
            }

            return $value;
        }
    }
}
